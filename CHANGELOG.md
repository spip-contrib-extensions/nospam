# Changelog du plugin NoSpam

## 3.0.0 - 2025-02-25

### Changed

- Compatible SPIP 4.2 minimum.